package gosila

import "github.com/google/uuid"

type MinimalServerConfig struct {
	Name        string
	UUID        string
	Type        string
	Description string
	Version     string
	VendorUrl   string
	Port        string
}

type ServerConfig struct {
	MinimalServerConfig
	UseTLS      bool
	TlsCertPath string
	TlsKeyPath  string
}

func NewServerConfig(name, _type, description, version, vendorURL, port, certPath, keyPath string) ServerConfig {
	return ServerConfig{
		MinimalServerConfig: MinimalServerConfig{
			Name:        name,
			UUID:        uuid.NewString(),
			Type:        _type,
			Description: description,
			Version:     version,
			VendorUrl:   vendorURL,
			Port:        port,
		},
		UseTLS:      true,
		TlsCertPath: certPath,
		TlsKeyPath:  keyPath,
	}
}

func NewServerWithoutTLS(name, _type, description, version, vendorURL, port string) ServerConfig {
	return ServerConfig{
		MinimalServerConfig: MinimalServerConfig{
			Name:        name,
			UUID:        uuid.NewString(),
			Type:        _type,
			Description: description,
			Version:     version,
			VendorUrl:   vendorURL,
			Port:        port,
		},
		UseTLS:      false,
		TlsCertPath: "",
		TlsKeyPath:  "",
	}
}
