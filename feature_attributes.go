package gosila

import (
	"fmt"
	"regexp"
	"strconv"
)

type MaturityLevel string

const (
	Draft     MaturityLevel = "Draft"
	Verified  MaturityLevel = "Verified"
	Normative MaturityLevel = "Normative"
)

func validateMaturityLevel(level MaturityLevel) error {
	switch level {
	case Draft, Verified, Normative:
		return nil
	default:
		return NewGosilaError(InvalidMaturityLevelError, fmt.Sprintf("invalid Maturity Level: %s", level))
	}
}

type Version struct {
	Major int
	Minor int
}

func parseVersion(input string) (Version, error) {
	r := regexp.MustCompile(`^(\d+)\.(\d+)$`)
	matches := r.FindStringSubmatch(input)
	if matches == nil {
		return Version{}, NewGosilaError(InvalidVersionError, fmt.Sprintf("%s is not a valid version"))
	}

	major, err := strconv.Atoi(matches[1])
	if err != nil {
		return Version{}, fmt.Errorf("error converting major version to integer: %v", err)
	}

	minor, err := strconv.Atoi(matches[2])
	if err != nil {
		return Version{}, fmt.Errorf("error converting minor version to integer: %v", err)
	}

	return Version{Major: major, Minor: minor}, nil
}

// TODO do some validation/ checking against the best practices here
// TODO cache the result of command and feature identifier generation after the first creation

type FeatureAttributes struct {
	SiLA2Version   Version
	FeatureVersion Version
	MaturityLevel  MaturityLevel
	Originator     string
	Category       string
}

type FeatureOptFunc func(attributes *FeatureAttributes)

func NewFeatureAttributes(originator string, opts ...FeatureOptFunc) FeatureAttributes {

	featureAttributes := FeatureAttributes{
		SiLA2Version: Version{
			Major: 1,
			Minor: 1,
		},
		FeatureVersion: Version{
			1,
			0,
		},
		MaturityLevel: Draft,
		Category:      "None",
	}

	for _, optFunc := range opts {
		optFunc(&featureAttributes)
	}

	return featureAttributes
}

func WithSiLA2Version(major, minor int) FeatureOptFunc {
	return func(attributes *FeatureAttributes) {
		attributes.SiLA2Version = Version{major, minor}
	}
}

func WithVersion(major, minor int) FeatureOptFunc {
	return func(attributes *FeatureAttributes) {
		attributes.FeatureVersion = Version{major, minor}
	}
}

func WithMaturityLevel(level MaturityLevel) FeatureOptFunc {
	return func(attributes *FeatureAttributes) {
		attributes.MaturityLevel = level
	}
}

func WithCategory(category string) FeatureOptFunc {
	return func(attributes *FeatureAttributes) {
		attributes.Category = category
	}
}
