// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: SilaCloudConnector.proto

package proto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CloudClientEndpointClient is the client API for CloudClientEndpoint service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CloudClientEndpointClient interface {
	// connect a server to stream messages
	ConnectSiLAServer(ctx context.Context, opts ...grpc.CallOption) (CloudClientEndpoint_ConnectSiLAServerClient, error)
}

type cloudClientEndpointClient struct {
	cc grpc.ClientConnInterface
}

func NewCloudClientEndpointClient(cc grpc.ClientConnInterface) CloudClientEndpointClient {
	return &cloudClientEndpointClient{cc}
}

func (c *cloudClientEndpointClient) ConnectSiLAServer(ctx context.Context, opts ...grpc.CallOption) (CloudClientEndpoint_ConnectSiLAServerClient, error) {
	stream, err := c.cc.NewStream(ctx, &CloudClientEndpoint_ServiceDesc.Streams[0], "/sila2.org.silastandard.CloudClientEndpoint/ConnectSiLAServer", opts...)
	if err != nil {
		return nil, err
	}
	x := &cloudClientEndpointConnectSiLAServerClient{stream}
	return x, nil
}

type CloudClientEndpoint_ConnectSiLAServerClient interface {
	Send(*SiLAServerMessage) error
	Recv() (*SiLAClientMessage, error)
	grpc.ClientStream
}

type cloudClientEndpointConnectSiLAServerClient struct {
	grpc.ClientStream
}

func (x *cloudClientEndpointConnectSiLAServerClient) Send(m *SiLAServerMessage) error {
	return x.ClientStream.SendMsg(m)
}

func (x *cloudClientEndpointConnectSiLAServerClient) Recv() (*SiLAClientMessage, error) {
	m := new(SiLAClientMessage)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// CloudClientEndpointServer is the server API for CloudClientEndpoint service.
// All implementations must embed UnimplementedCloudClientEndpointServer
// for forward compatibility
type CloudClientEndpointServer interface {
	// connect a server to stream messages
	ConnectSiLAServer(CloudClientEndpoint_ConnectSiLAServerServer) error
	mustEmbedUnimplementedCloudClientEndpointServer()
}

// UnimplementedCloudClientEndpointServer must be embedded to have forward compatible implementations.
type UnimplementedCloudClientEndpointServer struct {
}

func (UnimplementedCloudClientEndpointServer) ConnectSiLAServer(CloudClientEndpoint_ConnectSiLAServerServer) error {
	return status.Errorf(codes.Unimplemented, "method ConnectSiLAServer not implemented")
}
func (UnimplementedCloudClientEndpointServer) mustEmbedUnimplementedCloudClientEndpointServer() {}

// UnsafeCloudClientEndpointServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CloudClientEndpointServer will
// result in compilation errors.
type UnsafeCloudClientEndpointServer interface {
	mustEmbedUnimplementedCloudClientEndpointServer()
}

func RegisterCloudClientEndpointServer(s grpc.ServiceRegistrar, srv CloudClientEndpointServer) {
	s.RegisterService(&CloudClientEndpoint_ServiceDesc, srv)
}

func _CloudClientEndpoint_ConnectSiLAServer_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(CloudClientEndpointServer).ConnectSiLAServer(&cloudClientEndpointConnectSiLAServerServer{stream})
}

type CloudClientEndpoint_ConnectSiLAServerServer interface {
	Send(*SiLAClientMessage) error
	Recv() (*SiLAServerMessage, error)
	grpc.ServerStream
}

type cloudClientEndpointConnectSiLAServerServer struct {
	grpc.ServerStream
}

func (x *cloudClientEndpointConnectSiLAServerServer) Send(m *SiLAClientMessage) error {
	return x.ServerStream.SendMsg(m)
}

func (x *cloudClientEndpointConnectSiLAServerServer) Recv() (*SiLAServerMessage, error) {
	m := new(SiLAServerMessage)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// CloudClientEndpoint_ServiceDesc is the grpc.ServiceDesc for CloudClientEndpoint service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CloudClientEndpoint_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "sila2.org.silastandard.CloudClientEndpoint",
	HandlerType: (*CloudClientEndpointServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "ConnectSiLAServer",
			Handler:       _CloudClientEndpoint_ConnectSiLAServer_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "SilaCloudConnector.proto",
}
