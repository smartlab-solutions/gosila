package main

import (
	"context"
	_ "embed"
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/smartlab-solutions/gosila"
	pb "gitlab.com/smartlab-solutions/gosila/proto"
	"log"
	"os"
)

// com.smartlabsolutions/devices/BalanceService/v1

// GreetingProvider is the implementation of the feature defined in `GreetingProvider-v1.0.sila.xml` file.
type GreetingProvider struct {
	pb.UnimplementedGreetingProviderServer
}

func (s *GreetingProvider) SayHello(ctx context.Context, parameters *pb.SayHello_Parameters) (*pb.SayHello_Responses, error) {
	return &pb.SayHello_Responses{
		Greeting: &pb.String{Value: fmt.Sprintf("Hello %s", parameters.Name)},
	}, nil
}

func (s *GreetingProvider) Get_StartYear(ctx context.Context, parameters *pb.Get_StartYear_Parameters) (*pb.Get_StartYear_Responses, error) {
	return &pb.Get_StartYear_Responses{
		StartYear: &pb.Integer{Value: 2023},
	}, nil
}

//go:embed GreetingProvider-v1.0.sila.xml
var greetingProviderDefinition string

func main() {
	// configuration via .env file in the same working directory as the executable
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	certPath := os.Getenv("CERTPATH")
	keyPath := os.Getenv("KEYPATH")

	config := gosila.NewServerConfig(
		"MyServer",
		"YourType",
		"An example server providing a GreetingService implementation",
		"1.0",
		"https://smartlab-solutions.de",
		"50052",
		certPath,
		keyPath,
	)

	srv, err := gosila.NewSilaServer(config)

	if err != nil {
		log.Fatal(err)
	}

	err = gosila.RegisterXMLFeature[pb.GreetingProviderServer](srv, greetingProviderDefinition, pb.RegisterGreetingProviderServer, &GreetingProvider{})

	if err != nil {
		log.Fatal(err)
	}

	srv.Run()
}
