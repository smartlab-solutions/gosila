package gosila

import (
	"crypto/tls"
	"gitlab.com/smartlab-solutions/gosila/proto"
	"google.golang.org/grpc/credentials"
)

func ProtoString(input string) *proto.String {
	return &proto.String{
		Value: input,
	}
}

func loadTLSCredentials(certPath, keyPath string) (credentials.TransportCredentials, error) {
	// Load server's certificate and private key
	serverCert, err := tls.LoadX509KeyPair(certPath, keyPath)
	if err != nil {
		return nil, err
	}

	// Create the credentials and return it
	config := &tls.Config{
		Certificates: []tls.Certificate{serverCert},
		ClientAuth:   tls.NoClientCert,
	}

	return credentials.NewTLS(config), nil
}
