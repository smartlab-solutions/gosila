package gosila

import "fmt"

type ErrorType string

const (
	InvalidVersionError       ErrorType = "InvalidVersionError"
	InvalidMaturityLevelError ErrorType = "InvalidMaturityLevelError"
)

type GosilaError struct {
	msg       string
	errorType ErrorType
	cause     error
}

func NewGosilaError(errorType ErrorType, msg string) GosilaError {
	return GosilaError{
		msg:       msg,
		errorType: errorType,
	}
}

func WrapAsGosilaError(cause error, errorType ErrorType, msg string) GosilaError {
	ge := NewGosilaError(errorType, msg)
	ge.cause = cause
	return ge
}

func (ge GosilaError) AsError() error {
	baseMsg := fmt.Sprintf("%s: %s", ge.errorType, ge.msg)
	if ge.cause != nil {
		return fmt.Errorf("%s, caused by %w", baseMsg, ge.cause)
	}

	return fmt.Errorf(baseMsg)
}

func (ge GosilaError) Error() string {
	return ge.AsError().Error()
}
