package gosila

import (
	"encoding/xml"
	"fmt"
)

type Feature struct {
	Identifier  string
	DisplayName string
	Description string

	attr FeatureAttributes
}

func NewFeature(identifier, displayName, description, originator string, opts ...FeatureOptFunc) Feature {
	attr := NewFeatureAttributes(originator, opts...)

	return Feature{
		Identifier:  identifier,
		DisplayName: displayName,
		Description: description,
		attr:        attr,
	}
}

func (fa Feature) FullyQualifiedFeatureIdentifier() string {
	return fmt.Sprintf("%s/%s/%s/v%d", fa.attr.Originator, fa.attr.Category, fa.Identifier, fa.attr.FeatureVersion.Major)
}

func (fa Feature) FullyQualifiedCommandIdentifier(identifier string) string {
	return fmt.Sprintf("%s/Command/%s", fa.FullyQualifiedFeatureIdentifier(), identifier)
}

func (fa Feature) FullyQualifiedCommandResponseIdentifier(identifier string) string {
	return fmt.Sprintf("%s/Response/%s", fa.FullyQualifiedCommandIdentifier(identifier), identifier)
}

func (fa Feature) FullyQualifiedIntermediateCommandResponseIdentifier(identifier string) string {
	return fmt.Sprintf("%s/IntermediateResponse/%s", fa.FullyQualifiedCommandIdentifier(identifier), identifier)
}

func (fa Feature) FullyQualifiedDefinedExecutionErrorIdentifier(identifier string) string {
	return fmt.Sprintf("%s/DefinedExecutionError/%s", fa.FullyQualifiedFeatureIdentifier(), identifier)
}

func (fa Feature) FullyQualifiedPropertyIdentifier(identifier string) string {
	return fmt.Sprintf("%s/Property/%s", fa.FullyQualifiedFeatureIdentifier(), identifier)
}

func (fa Feature) FullyQualifiedCustomDataTypeIdentifier(identifier string) string {
	return fmt.Sprintf("%s/DataType/%s", fa.FullyQualifiedFeatureIdentifier(), identifier)
}

func (fa Feature) FullyQualifiedMetadataIdentifier(identifier string) string {
	return fmt.Sprintf("%s/Metadata/%s", fa.FullyQualifiedFeatureIdentifier(), identifier)
}

// XMLFeature is an intermediate representation that is serialized directly from the feature definition XML (the source
// of truth according to the standard). It reads the XML, performs some basic validations and converts it to the actual
// Feature.
type XMLFeature struct {
	XMLName        xml.Name      `xml:"Feature"`
	Category       string        `xml:"Category,attr"`
	FeatureVersion string        `xml:"FeatureVersion,attr"`
	MaturityLevel  MaturityLevel `xml:"MaturityLevel,attr"`
	Originator     string        `xml:"Originator,attr"`
	SiLA2Version   string        `xml:"SiLA2Version,attr"`
	Identifier     string        `xml:"Identifier"`
	DisplayName    string        `xml:"DisplayName"`
	Description    string        `xml:"Description"`
}

func (x XMLFeature) ToFeature() (Feature, error) {

	silaVersion, err := parseVersion(x.SiLA2Version)
	if err != nil {
		return Feature{}, err
	}

	featureVersion, err := parseVersion(x.FeatureVersion)
	if err != nil {
		return Feature{}, err
	}

	if err = validateMaturityLevel(x.MaturityLevel); err != nil {
		return Feature{}, err
	}

	return Feature{
		Identifier:  x.Identifier,
		DisplayName: x.DisplayName,
		Description: x.Description,
		attr: FeatureAttributes{
			SiLA2Version:   silaVersion,
			FeatureVersion: featureVersion,
			MaturityLevel:  x.MaturityLevel,
			Originator:     x.Originator,
			Category:       x.Category,
		},
	}, nil
}
