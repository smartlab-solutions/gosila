package gosila

import (
	_ "embed"
	"encoding/xml"
	"fmt"
	"gitlab.com/smartlab-solutions/gosila/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

//go:embed features/SiLAService.sila.xml
var serverDefinition string

type SilaServer struct {
	ServerConfig
	proto.UnimplementedSiLAServiceServer
	featureDefinitions map[string]string
	grpcServer         *grpc.Server
	listener           net.Listener
}

func NewSilaServer(config ServerConfig) (*SilaServer, error) {

	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", config.Port))
	if err != nil {
		return nil, err
	}

	var s *grpc.Server

	if config.UseTLS {
		tlsCredentials, err := loadTLSCredentials(config.TlsCertPath, config.TlsKeyPath)
		if err != nil {
			return nil, err
		}

		s = grpc.NewServer(grpc.Creds(tlsCredentials))
	} else {
		s = grpc.NewServer()
	}

	srv := &SilaServer{
		ServerConfig:       config,
		featureDefinitions: make(map[string]string),
		grpcServer:         s,
		listener:           lis,
	}

	srv.registerSilaServer()

	return srv, nil
}

// RegisterXMLFeature is a convenience function that simplifies the SiLA service registration by parsing the XML file for the required feature
// metadata (originator, version ...) and adds them to the server automatically.
func RegisterXMLFeature[T any](si *SilaServer, xmlFeatureDefinition string, registerFunc func(s grpc.ServiceRegistrar, impl T), impl T) error {
	var xmlFeature XMLFeature
	err := xml.Unmarshal([]byte(xmlFeatureDefinition), &xmlFeature)
	if err != nil {
		return err
	}

	feature, err := xmlFeature.ToFeature()
	if err != nil {
		return err
	}
	RegisterFeature(si, feature.FullyQualifiedFeatureIdentifier(), xmlFeatureDefinition, registerFunc, impl)

	return nil
}

func RegisterFeature[T any](si *SilaServer, fullQualifiedName, xmlFeatureDefinition string, registerFunc func(s grpc.ServiceRegistrar, impl T), impl T) {
	registerFunc(si.grpcServer, impl)
	si.featureDefinitions[fullQualifiedName] = xmlFeatureDefinition
}

func (si *SilaServer) registerSilaServer() {
	//proto.RegisterSiLAServiceServer(si.grpcServer, si)
	RegisterFeature[proto.SiLAServiceServer](si, "org.silastandard/core/SiLAService/v1", serverDefinition, proto.RegisterSiLAServiceServer, si)
}

func (si *SilaServer) Run() {
	if err := si.grpcServer.Serve(si.listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

// TODO constructor with reasonable defaults

// 1. Require TLS
// 2. Register feature
