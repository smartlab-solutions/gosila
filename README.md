# gosila: SilA 2 implementation in Go

## Status

This project is under development and by no means feature-complete, any contributions, ideas and suggestions are
more than welcome. The most obvious flaws:

1. At the moment, only server-side code has been written. The client is still a big TODO
2. The project does not contain any translation tool from feature definition XML to protocol buffers

## Introduction

Implementation of the [SiLA 2 standard](https://sila-standard.com/) using Golang. Go has some advantages that make at
an interesting candidate to use it in the SiLA/ lab digitalization context:

1. Designed for highly concurrent micro-and webservices, excellent gRPC support
2. Easy deployment: usually one single small statically linked executable as output
3. Statically typed, yet simple to use
4. Very suitable for cloud-native

This project is a trial and serves both educational purposes and to provide a starting point for anybody who is
willing to give Go a try to interact with laboratory devices.


## Contribution /Development

Every input and contribution, e.g. feature and pull requests, issues and bug reports, are very welcome.

### Feature definition XML to protobuf

At the moment, this project does not include a compiler or code generator that transforms SiLA 2 feature definitions to protobuf files. 
It is recommended to use either the XLST transformations provided at [the SiLA2 GitLab repo](https://gitlab.com/SiLA2/sila_base/-/tree/master/xslt?ref_type=heads),
invoking 

```sh
xlstproc fdl2proto.xsl <input_feature_definition> > <output_proto>
```
or the excellent [tooling of the Python SiLA 2 implementation](https://gitlab.com/SiLA2/sila_python). 

### Compiling the protocol buffers
This repo contains a compiled version of the protobufs. However, if it is necessary to re-compile them, inside 
the `/proto` directory, run:

```
protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
```