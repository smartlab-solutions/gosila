package gosila

import "fmt"

type Originator struct {
	Slug string
	Name string
}

func (o Originator) String() string {
	return fmt.Sprintf("%s.%s", o.Slug, o.Name)
}
