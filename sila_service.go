package gosila

import (
	"context"
	"gitlab.com/smartlab-solutions/gosila/proto"
	"log"
)

func (si *SilaServer) GetFeatureDefinition(ctx context.Context, parameters *proto.GetFeatureDefinition_Parameters) (*proto.GetFeatureDefinition_Responses, error) {

	log.Printf("%s was requested\n", parameters.FeatureIdentifier.Value)

	definition, ok := si.featureDefinitions[parameters.FeatureIdentifier.Value]

	if !ok {
		log.Fatalf("unknown feature")
	}

	return &proto.GetFeatureDefinition_Responses{
		FeatureDefinition: ProtoString(definition),
	}, nil
}

func (si *SilaServer) SetServerName(ctx context.Context, parameters *proto.SetServerName_Parameters) (*proto.SetServerName_Responses, error) {
	si.Name = parameters.ServerName.String()
	return &proto.SetServerName_Responses{}, nil
}

func (si *SilaServer) Get_ServerName(ctx context.Context, parameters *proto.Get_ServerName_Parameters) (*proto.Get_ServerName_Responses, error) {
	return &proto.Get_ServerName_Responses{
		ServerName: ProtoString(si.Name),
	}, nil
}

func (si *SilaServer) Get_ServerType(ctx context.Context, parameters *proto.Get_ServerType_Parameters) (*proto.Get_ServerType_Responses, error) {
	return &proto.Get_ServerType_Responses{
		ServerType: ProtoString(si.Type),
	}, nil

}

func (si *SilaServer) Get_ServerUUID(ctx context.Context, parameters *proto.Get_ServerUUID_Parameters) (*proto.Get_ServerUUID_Responses, error) {
	return &proto.Get_ServerUUID_Responses{
		// TODO: the generation of this has to be implemented in a reasonable manner
		ServerUUID: ProtoString(si.UUID),
	}, nil
}

func (si *SilaServer) Get_ServerDescription(ctx context.Context, parameters *proto.Get_ServerDescription_Parameters) (*proto.Get_ServerDescription_Responses, error) {
	return &proto.Get_ServerDescription_Responses{
		ServerDescription: &proto.String{Value: si.Description},
	}, nil
}

func (si *SilaServer) Get_ServerVersion(ctx context.Context, parameters *proto.Get_ServerVersion_Parameters) (*proto.Get_ServerVersion_Responses, error) {
	return &proto.Get_ServerVersion_Responses{
		ServerVersion: ProtoString(si.Version),
	}, nil
}

func (si *SilaServer) Get_ServerVendorURL(ctx context.Context, parameters *proto.Get_ServerVendorURL_Parameters) (*proto.Get_ServerVendorURL_Responses, error) {
	return &proto.Get_ServerVendorURL_Responses{
		ServerVendorURL: ProtoString(si.VendorUrl),
	}, nil
}

func (si *SilaServer) Get_ImplementedFeatures(ctx context.Context, parameters *proto.Get_ImplementedFeatures_Parameters) (*proto.Get_ImplementedFeatures_Responses, error) {

	implementedFeatures := make([]*proto.String, 0, len(si.featureDefinitions))
	for key, _ := range si.featureDefinitions {
		implementedFeatures = append(implementedFeatures, ProtoString(key))
	}

	return &proto.Get_ImplementedFeatures_Responses{
		ImplementedFeatures: implementedFeatures,
	}, nil
}
